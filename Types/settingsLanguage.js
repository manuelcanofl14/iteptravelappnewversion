
export const Settings = [
    {
        title:"ajustes",
        ES:"Ajustes",
        EN:"Settings"
    },
    {
        title:"cambiarIdioma",
        ES:"SELECCIONAR IDIOMA",
        EN:"SELECT LANGUAGE"
    },
    {
        title:"empezar",
        ES:"Ver Todos Nuestros Tours",
        EN:"View Our Tours"
    },
    //Tours titles
    {   
        title:'Adventure',
        ES: 'Tours de Aventura',
        EN: 'Trekking Tours'
    },
    {
        title:'Cultural',
        ES: 'Tours Culturales', 
        EN: 'Cultural Tours' 
    },
    {
        title:'Vivential Tourism',
        ES: 'Turismo Vivencial',
        EN: 'Vivential Tourism'
    },
    {
        title:'Mistic Experience',
        ES: 'Experiencias Misticas', 
        EN: 'Mistic Experience'  
    },
    {
        title:'Traditional',
        ES: 'Tours Tradicionales',
        EN: 'Traditional Tours'
    },
    {
        title:'Others',
        ES: 'Otros Tours', 
        EN: 'Other Tours'
    },

    //tour screen titles
    {
        title:'ver',
        ES: 'VER MAS', 
        EN: 'VIEW MORE'
    },
    {
        title:'overview',
        ES: 'Detalles', 
        EN: 'Overview'
    },
    {
        title:'descripciones',
        ES: 'Descripción', 
        EN: 'Description'
    },
    {
        title:'incluye',
        ES: 'Incluye', 
        EN: 'Include'
    },
    {
        title:'noIncluye',
        ES: 'No Incluye', 
        EN: 'Not Included'
    },
    {
        title:'necesita',
        ES: 'Necesita Llevar', 
        EN: 'Need to Bring'
    },
    {
        title:'informacion',
        ES: 'Información Importante', 
        EN: 'Important Information'
    },
    {
        title:'dia',
        ES:'Dia(s)',
        EN:'Day(s)'
    },
    {
        title:'noche',
        ES:'Noche(s)',
        EN:'Night(s)'
    },
    {
        title:'selectCatg',
        ES:'seleccionar categoría',
        EN:'select category'
    },
    {
        title:'popDest',
        ES:'Destinos Populares',
        EN:'Popular Destinations'
    },
    {
        title:'seeAll',
        ES:'Ver Todo',
        EN:'See All'
    },
    {
        title:'meinT',
        ES:'Tours Guardados',
        EN:'Selected Tours'
    },


    // voucher main screen titles
    {
        title:'ingreseEmail',
        ES:'Ingrese su email',
        EN:'Enter your email'
    },
    {
        title:'buscarBoton',
        ES:'Buscar',
        EN:'Search'
    },
    {
        title:'detallesPasajeros',
        ES:'Detalles de pasajeros',
        EN:'Passegger Details'
    },
    {
        title:'detallesPagos',
        ES:'Detalles de pago',
        EN:'Payment Details'
    },
    {
        title:'informacionServicio',
        ES:'Información del servicio',
        EN:'Service Information'
    },
    {
        title:'detallesTour',
        ES:'Detalles Tour',
        EN:'Tour Details'
    },
    {
        title:'voucher',
        ES:'Voucher de Servicios',
        EN:'Voucher of Services'
    },
   

    //voucher details
    {
        title:'codigo',
        ES:'CÓDIGO DE VOUCHER',
        EN: 'VOUCHER CODE'
    },
    {
        title:'agencia',
        ES:'Agencia',
        EN:'Agency'
    },
    {
        title:'lider',
        ES:'Líder de Viaje',
        EN:'Tour Leader'
    },
    {
        title:'fecha',
        ES:'Fecha de Salida',
        EN:'Date of Departure'
    },

    //passenger details titles

    {
        title:'apellidos',
        ES:'Apellidos',
        EN:'Surname'
    },
    {
        title:'genero',
        ES:'Genero',
        EN:'Gender'
    },
    {
        title:'nacimiento',
        ES:'Fecha de Nacimiento',
        EN:'Name'
    },
    {
        title:'pais',
        ES:'País',
        EN:'Country'
    },
    {
        title:'edad',
        ES:'Edad',
        EN:'Age'
    },
    {
        title:'pasaporte',
        ES:'Pasaporte',
        EN:'Passport'
    },
    {
        title:'dieta',
        ES:'Dieta',
        EN:'Dietary'
    },
    {
        title:'medical',
        ES:'Condición Médica',
        EN:'Medical'
    },

    // Payment details screen titles

    {
        title:'buscar',
        ES: 'Buscar Voucher',
        EN: 'Search for a Voucher'
    },
    {
        title:'cantidadPago',
        ES: 'Monto Total',
        EN: 'Total Amount'
    },
    {
        title:'cantidad',
        ES:'Cantidad',
        EN:'Quantity'
    },
    {
        title:'detallesPago',
        ES: 'Detalle',
        EN: 'Detail'
    },
    {
        title:'descripcion',
        ES:'Descripción',
        EN:'Description'
    },
    {
        title:'precioPago',
        ES: 'Precio por item',
        EN: 'Price per item'
    },
    {
        title:'precioSubtotal',
        ES: 'Subtotal',
        EN: 'Subtotal'
    },
    {
        title:'deposito',
        ES:'Depósito',
        EN:'Deposit'
    },
    {
        title:'balance',
        ES:'Balance',
        EN:'Balance'
    },
    {
        title:'emision',
        ES: 'Emisión',
        EN: 'Emission'
    },
    {
        title:'trip',
        ES: 'Viaje',
        EN: 'Trip'
    },
    {
        title:'tipotour',
        ES: 'tipo de tour',
        EN: 'tour type'
    },
    {
        title:'tiposervi',
        ES: 'tipo de servicio',
        EN: 'type of service'
    },
    {
        title:'adicional',
        ES: 'adicional',
        EN: 'aditional'
    },
    {
        title:'language',
        ES: 'idioma',
        EN: 'language'
    },
    {
        title:'observacion',
        ES: 'observaciones',
        EN: 'observations'
    },
    {
        title:'Tinformacion',
        ES: 'Tour Information',
        EN: 'Información del Tour'
    }, 
    {
        title:'horario',
        ES: 'Horario',
        EN: 'Schedule'
    },
    {
        title:'fechain',
        ES: 'Fecha de arribo',
        EN: 'date in'
    },
    {
        title:'fechasal',
        ES: 'fecha de salida',
        EN: 'date out'
    },
    {
        title:'pickup',
        ES: 'hora de recojo',
        EN: 'pick up time'
    },
    
    {
        title:'hospe',
        ES: 'hospedaje',
        EN: 'accomodation'
    },
   

    //Asistencia
    {
        title:'errorMsg',
        ES:'Asegurese de que Whatsapp esté instalado.',
        EN:'Make sure WhatsApp installed on your device.'
    },
    {
        title:'mensaje',
        ES:'Hola, necesito asistencia, por favor.',
        EN:'Hello, I need assistance, please.'
    },
    {
        title:'inicioAssist',
        ES:'Somos Itep Travel, una agencia de viajes con base en Cusco, especializada en brindar experiencias personalizadas así sean tradicionales o de aventura.',
        EN:'We are ITEP TRAVEL, a travel agency based in Cusco specialized in provide customized experiences whether traditional or adventure.'
    },
    {
        title:'assist',
        ES:'Asistencia',
        EN:'Assistance'
    },
    {
        title:'conect',
        ES:'Enviar whatsapp',
        EN:'Send message'
    },
    {
        title:'ubicacion',
        ES:'Nuestra Ubicación',
        EN:'Our Location'
    },
    {
        title:'contacto',
        ES:'Contáctenos',
        EN:'Contact Us'
    },
    {
        title:'resenas',
        ES:'Opiniones',
        EN:'Our Reviews'
    },
    {
        title:'whatsapp',
        ES:'Mensaje a Whatsapp',
        EN:'Whatsapp Message'
    },
   
    
    {
        title:'errorVoucher',
        ES:'El correo no esta registrado, intenta con otro',
        EN:'The provided email is not registered'
    },
    {
        title:'errorPagos',
        ES:'No se encontraron datos de pago',
        EN:'No payment details for this voucher'
    },
    {
        title:'errorDetalles',
        ES:'No se encontraron datos de detalles de tour',
        EN:'No tour details found for this voucher'
    },
    {
        title:'entendido',
        ES:'Entendido',
        EN:'Ok'
    },
    {
        title: 'errorInput',
        ES: 'Por favor, escriba algo !',
        EN:'Please type something !'
    },
    {title:'resultadoBusq',
    ES:'Hemos encontrado',
    EN:'We have found'
    }
]
   
function ajustarIdioma (idioma,titulo){
    const ajusteIdioma = Settings.find((item)=>{
            return (item.title === titulo)
    })
    switch(idioma){
        case 'ENGLISH':
            return ajusteIdioma.EN
        case 'ESPAÑOL':
            return ajusteIdioma.ES
        default : return ajusteIdioma.EN
    }
}

export default ajustarIdioma