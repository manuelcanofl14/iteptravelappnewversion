import React, {useEffect} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import LanguageScreen from './Screens/StartScreens/LanguageScreen'

//components 
import ToursState from './ToursAPI/toursState'
import mainTab from './Navigators/Tab/MainTab';

import SplashScreen from 'react-native-splash-screen'

//start stack navigator created 
const mainStack = createNativeStackNavigator();


const App = () => {

  useEffect(()=>{
    SplashScreen.hide()
  },[])


  return (
    <ToursState>
      <NavigationContainer
        
      >
        <mainStack.Navigator
          initialRouteName="language"
        >
          <mainStack.Screen 
            name="language" 
            component={LanguageScreen}
            options={
              {
                headerShown:false
              }
            }
            />
          <mainStack.Screen  
            name="start" 
            component={mainTab}
            options={
              {
                headerShown:false
              }
            }
            />
        </mainStack.Navigator>
        
      </NavigationContainer>
    </ToursState>
  )
};

export default App;
