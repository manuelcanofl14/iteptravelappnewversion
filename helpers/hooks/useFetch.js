//https://www.incatrail.org/rest/voucher.php?number=NCA00051
//https://www.incatrail.org/rest/passenger.php?idVoucher=5a5654f006621
//https://www.incatrail.org/rest/information.php?idVoucher=5a5654f006621
//https://www.incatrail.org/rest/payment.php?idVoucher=5a3a20571e2af

import axios from 'axios'
import { useEffect, useState } from 'react'


export const useFetch = (categoria,parametro,valor) =>{
    const [datos, setDatos] = useState({
        data:null,
        error: true
    })
     useEffect( ()=>{
        const getData = async()=>{
            try {
                const urlData = `https://www.incatrail.org/rest/${categoria}.php?${parametro}=${valor}`
            const  {data} = await axios.get(urlData)
            setDatos({
                data,
                error:false
            })
            } catch (error) {
               console.log( 'fetch error',error)
            }  
        }
        getData()
     },[valor])
        
    return datos
}   
        