import { Linking } from 'react-native'

const openWhatsApp = (msg, mobile, errorMsg) => {
    
        let url =
          "whatsapp://send?text=" +
          msg +
          "&phone=" +
          mobile;
        Linking.openURL(url)
          .then(data => {
            console.log("WhatsApp Opened successfully " + data);
          })
          .catch(() => {
            alert(errorMsg);
          });
      }

 export default openWhatsApp