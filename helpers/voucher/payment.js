const depositCalculate = (listPayments)=>{
    let total = 0
    listPayments.forEach((deposit)=>{
        total += Number(deposit.deposito)
     })
    return total
}

export const subtotalCalculate = (listPayments)=>{
    let subTotal = 0
    listPayments.forEach((deposit)=>{
        subTotal += Number(deposit.subtotal)
     })
    return subTotal
}

export default depositCalculate