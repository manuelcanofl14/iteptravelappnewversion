import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import VoucherConsult from '../../Screens/VoucherScreens/VoucherConsult';
import VoucherConsultOptions from '../../Screens/VoucherScreens/VoucherConsultOptions';
import VoucherDetails from '../../Screens/VoucherScreens/VoucherDetails'
import PassengerDetails from '../../Screens/VoucherScreens/PassengerDetails';
import PassengerList from '../../Screens/VoucherScreens/PassengerList';
import PassengerPayment from '../../Screens/VoucherScreens/PassengerPayment';
import VoucherInformation from '../../Screens/VoucherScreens/VoucherInformation';

const voucherStack = createNativeStackNavigator();

const voucherStackNavigation = ()=>{
    return(
        <voucherStack.Navigator
          initialRouteName='voucherConsult'
        >
            <voucherStack.Screen
                name="voucherConsult"
                component= {VoucherConsult}
                options={
                  {
                    headerShown:false,
                    headerTransparent:true
                  }
                }
            />
            <voucherStack.Screen
                name="voucherConsultOptions"
                component= {VoucherConsultOptions}
                options={
                  {
                    headerShown:false,
                    headerTransparent:true
                  }
                }
            />
             <voucherStack.Screen
                name="voucherDetails"
                component= {VoucherDetails}
                options={
                  {
                    headerTintColor: '#FFFFFF',
                    headerShown:true,
                    headerTransparent:true
                  }
                }
            />
              <voucherStack.Screen
                name="passengerDetails"
                component= {PassengerDetails}
                options={
                  {
                    headerTintColor: '#FFFFFF',
                    headerShown:true,
                    headerTransparent:true
                  }
                }
            />
              <voucherStack.Screen
                name="passengerList"
                component= {PassengerList}
                options={
                  {
                    headerTintColor: '#FFFFFF',
                    headerTitleStyle:{color:'#C0958D'},
                    headerShown:true,
                    headerTransparent:true
                  }
                }
            />
              <voucherStack.Screen
                name="passengerPayment"
                component= {PassengerPayment}
                options={
                  {
                    headerTintColor: '#C0958D',
                    headerTitleStyle:{fontSize:15},
                    headerShown:true,
                    headerTransparent:true
                  }
                }
            />
             <voucherStack.Screen
                name="voucherInformation"
                component= {VoucherInformation}
                options={
                  {
                    headerTintColor: '#FFFFFF',
                    headerShown:true,
                    headerTransparent:true
                  }
                }
            />
        </voucherStack.Navigator>
    )
}

export default voucherStackNavigation

