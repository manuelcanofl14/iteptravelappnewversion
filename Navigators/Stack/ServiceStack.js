import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import AssistanceScreen from '../../Screens/AsistanceScreens/AssistanceScreen'
import WebViewScreen from '../../Screens/AsistanceScreens/WebViewScreen';

const serviceStack = createNativeStackNavigator()

const serviceStackNavigation = ()=>{
    return(
        <serviceStack.Navigator
            initialRouteName='assistanceScreen'
        >
            <serviceStack.Screen
                 name= "assistanceScreen"
                 component={AssistanceScreen}
                 options={
                   {
                     headerShown:false
                   }
                 }
            />
            <serviceStack.Screen
                 name= "webViewScreen"
                 component={WebViewScreen}
                 options={
                   {
                     headerShown:false
                   }
                 }
            />
                
        </serviceStack.Navigator>
    )
}

export default serviceStackNavigation