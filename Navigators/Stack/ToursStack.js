
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


//components
import MainTours from '../../Screens/ToursScreens/MainTours';
import Home from '../../Screens/StartScreens/Home';
//styles
import customStyles from '../../styles/styles';
import SearchTours from '../../Screens/ToursScreens/SearchTours';
import TourDetails from '../../Screens/ToursScreens/TourDetails';


//tours stack 
const toursStack = createNativeStackNavigator();


  const toursStackNavigation = ()=>{
    return(
  
        <toursStack.Navigator
        >
            <toursStack.Screen 
              name="home"
              component= {Home}
              options={
                {
                  headerShown:false
                }
              }
            />
            <toursStack.Screen 
              name="mainTours"
              component= {MainTours}
              options={
                {
                  headerTintColor: customStyles.colors.PRIMARY_RED,
                  headerStyle:{backgroundColor: customStyles.colors.OVERLAY_GRAY},
                  title: 'Tours'
                }
              }
            />
            <toursStack.Screen 
              name="searchTours"
              component={SearchTours}
              options={
                {
                  // headerTransparent:true,
                  headerTintColor: customStyles.colors.SECONDARY_RED,
                  headerStyle:{backgroundColor: customStyles.colors.OVERLAY_GRAY},
                  title: ''
                }
              }
            />
            <toursStack.Screen 
              name="TourDetails"
              component={TourDetails}
              detachInactiveScreens={false}
              options={
                {
                
                  headerTransparent:true,
                  title: '',
                  headerTintColor: customStyles.colors.OVERLAY_GRAY,
                  detachPreviousScreen: true
                }
              }
            />
        </toursStack.Navigator>
     
    )
  }

  export default toursStackNavigation