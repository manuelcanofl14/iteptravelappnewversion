import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

//components
import toursStackNavigation from '../Stack/ToursStack';
import voucherStackNavigation from '../Stack/VoucherStack';

//styles
import customStyles from '../../styles/styles';

//icons
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import serviceStackNavigation from '../Stack/ServiceStack';

const tabNavigator = createBottomTabNavigator()

const mainTab = ()=>{
    return(
      <tabNavigator.Navigator
        screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {

          if(route.name === "tours"){
            return <FontAwesome5 name="hiking" size={size} color={color} />
          }
          else if(route.name === "assistance"){
            return <MaterialIcons name="assistant" size={size} color={color} />
          }
          else if(route.name === "voucher"){
            return <Ionicons name="document-sharp" size={size} color={color} />
          }
          
        },
        tabBarActiveTintColor: customStyles.colors.PRIMARY_RED,
        tabBarInactiveTintColor: customStyles.colors.PRIMARY_GRAY,

      })}
      >
           
          <tabNavigator.Screen 
            name= "tours"
            component={toursStackNavigation}
            options={
              {
                headerShown:false
              }
            }
          />
          <tabNavigator.Screen 
            name= "assistance"
            component={serviceStackNavigation}
            options={
              {
                headerShown:false
              }
            }
          />
          
          <tabNavigator.Screen 
            name= "voucher"
            component={voucherStackNavigation}
            options={
              {
                headerShown:false
              }
            }
          />
      </tabNavigator.Navigator>
    )
  }

  export default mainTab