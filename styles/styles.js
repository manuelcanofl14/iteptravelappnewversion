const customStyles ={
    colors : {
        PRIMARY_RED : '#B40000',
        SECONDARY_RED: '#820202',
        PRIMARY_GRAY: '#AEADAD',
        SECONDARY_GRAY: '#F1F9FF',
        MEDIUM_GRAY: '#707070',
        OVERLAY_GRAY:'#eff1f3'

    },
    fonts: {
        FONT_BOLD_ITALIC: 'SegoeUI-Bold-Italic',
        FONT_BOLD:'SegoeUI-Bold',
        FONT_ITALIC: 'SegoeUI-Italic',
        FONT_REGULAR:'SegoeUI'
    }
}


export default customStyles
