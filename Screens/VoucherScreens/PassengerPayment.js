import React, {useContext} from 'react'
import { View, StyleSheet, Text,  FlatList, ActivityIndicator } from 'react-native'
import { useFetch } from '../../helpers/hooks/useFetch';
import customStyles from '../../styles/styles'
import CustomIcon from '../../UI/Icons/CustomIcon'
import depositCalculate, {subtotalCalculate} from '../../helpers/voucher/payment'
import DepositCard from '../../UI/voucher/DepositCard';
import ajustarIdioma from '../../Types/settingsLanguage'
import ToursContext from '../../ToursAPI/toursContext'

const PassengerPayment = ({route})=>{
    const {idioma} = useContext(ToursContext)
    const {voucherParams} = route.params
    const {idVoucher } = voucherParams
    const {data} = useFetch('payment','idVoucher',idVoucher)
    const titles = [
        ajustarIdioma(idioma,'cantidad'),
        ajustarIdioma(idioma,'deposito'),
        ajustarIdioma(idioma,'detallesPago'),
        ajustarIdioma(idioma,'descripcion'),
        ajustarIdioma(idioma,'precioPago'),
        ajustarIdioma(idioma,'precioSubtotal')]

    //constantes
    const deposit = data? depositCalculate(data) : 0
    const subtotal = data? subtotalCalculate(data) : 0

    const renderHeader =()=>{
        return(
            <>
                <View style={styles.viewHeader}>
                    <View style={styles.viewPrice}>
                        <CustomIcon name={'attach-money'} color={'#070606'} size={20}/>
                        <Text style={styles.textBalance}>
                            {deposit}
                        </Text>
                    </View>
                    <View style={styles.viewCircle}/>

                    <View style={styles.viewDeposit}>
                        <Text style={styles.textTotalAmount}>{ajustarIdioma(idioma,'cantidadPago')}</Text>
                        <Text style={[styles.textTotalAmount, {color:'#9E4938', fontSize:20}]}>
                            <CustomIcon name={'attach-money'} color={'#9E4938'} size={20}/>
                            {subtotal}
                        </Text>
                    </View>
                </View>
                
                <View style={styles.viewLine}/>

                <View style={{flexDirection:'row', justifyContent:'space-evenly', alignItems:'center'}}>
                        <View style={{flexDirection:'column', alignItems:'center'}}>
                            <View style={{flexDirection:'row'}}>
                                <CustomIcon name={'attach-money'} color={'#070606'} size={15}/>    
                                <Text style={[styles.textBalance,{fontSize:20}]}>{deposit}</Text>
                            </View>
                            <Text style={styles.textTotalAmount} >{ajustarIdioma(idioma,'deposito')}</Text>
                        </View>
                        
                        <View style={[styles.viewLine, {marginTop:0,height:100, width:1}]}/>

                        <View style={{flexDirection:'column', alignItems:'center'}}>
                            <View style={{flexDirection:'row'}}>
                                <CustomIcon name={'attach-money'} color={'#070606'} size={20}/>
                                <Text style={[styles.textBalance,{fontSize:20}]}>{ subtotal - deposit}</Text>
                            </View>
                            <Text style={styles.textTotalAmount} >{ajustarIdioma(idioma,'balance')}</Text>
                        </View>
                </View>
            </>
        )
    }

    
    return(
        < >
            {
              data !== null ?  
                <View >
            
                <FlatList
                    data={data}
                    renderItem={
                        ({item})=>{
                            return(
                                <DepositCard deposit={item} titles={titles} />
                            )   
                        }
                    }
                    keyExtractor={({id})=> id}
                    ListHeaderComponent ={renderHeader}
                />
               
                </View>
                :
                <View style={styles.container}>
                    <ActivityIndicator size="large" color={customStyles.colors.PRIMARY_RED} />
                </View>
            }    
        </>
    )
}

const styles = StyleSheet.create({   
    viewCircle:{
        position:'absolute',
        left:250,
        right:-50,
        top:0,
        bottom:0,
        height:150,
        backgroundColor: customStyles.colors.PRIMARY_RED,
        borderRadius:75
    },
    viewLine:{ 
        margin:20, 
        height:1, 
        backgroundColor:customStyles.colors.PRIMARY_RED
    },
    viewHeader:{
        flexDirection:'row',
        height:150
    },
    viewDeposit:{
        position:'absolute',
         left:'5%',
         top:'70%',  
         borderBottomColor:customStyles.colors.PRIMARY_RED  
    },
    viewPrice:{
        flex:2,
        alignItems:'center',
        flexDirection:'row',
        margin:10
    },
    textBalance:{
        fontFamily: customStyles.fonts.FONT_BOLD,
        color: '#070606',
        fontSize:25
    },
    textTotalAmount:{
        fontFamily:customStyles.fonts.FONT_BOLD,
        color: '#C0958D',
        fontSize: 15,
        marginBottom:5 
    },
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems:'center'
      }
})

export default PassengerPayment