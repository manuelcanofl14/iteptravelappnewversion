import React, {useContext} from 'react'
import { StyleSheet, Text, View, SafeAreaView, Pressable } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import customStyles from '../../styles/styles';
import Ionicons from 'react-native-vector-icons/Ionicons'
import ajustarIdioma from '../../Types/settingsLanguage'
import ToursContext from '../../ToursAPI/toursContext'

const Item = ({ option,nav, params }) => (
  <Pressable
    style={({ pressed }) => [
      {
        backgroundColor: pressed
          ? customStyles.colors.OVERLAY_GRAY
          : '#ffffff'
      },
      styles.item
    ]}
    onPress={()=>nav.navigate(option.screen, {voucherParams: params})}
  >
      <Text style={styles.title}>{option.title}</Text>
      <MaterialIcons name={'keyboard-arrow-right'} color={'#A3503F'} size={30}/>
  </Pressable>
);

const VoucherConsultOptions = ({navigation, route})=>{
    const {idioma} = useContext(ToursContext)
    const {voucher} = route.params
    const {idVoucher,number,agencia, name,departure,hotel,emission, email, titulo, tourDetalles} = voucher

    const paramsDetails = {
      idVoucher,
      number,
      agencia,
      name,
      departure,
      hotel,
      emission,
      titulo,
      tourDetalles,
      navigation
    }

    const DATA = [{title:ajustarIdioma(idioma, 'detallesPasajeros'), screen:"passengerList"},
           {title:ajustarIdioma(idioma, 'detallesPagos'), screen:"passengerPayment"},
           {title: ajustarIdioma(idioma, 'informacionServicio'), screen:"voucherInformation"},
           {title:ajustarIdioma(idioma, 'detallesTour'), screen:"voucherDetails"}]
    
    return(
        <>
           <SafeAreaView style={styles.container}>
                <View style={styles.ViewHeader}>
                  <View style={{alignItems:'center'}}>
                    <Text style={styles.textView}>{ajustarIdioma(idioma, 'lider')}</Text>
                    <Ionicons name={'md-person-circle'} color={'#ffffff'} size={150}/>
                    <Text style={styles.textView}>{name}</Text>
                    <Text style={styles.textEmail}>{email}</Text>
                  </View>
                </View>
                <View style={styles.viewList}>
    
                  {
                    DATA.map((item, index)=>{
                       return(
                        <Item  option={item} nav={navigation} params={paramsDetails}/>
                       ) 
                    })
                  }
                   
                </View>
               
            </SafeAreaView> 
        </>   
    )
}

const styles = StyleSheet.create({
  ViewHeader:{
      flex:1,
      height:'50%',
      width:'100%',
      backgroundColor: customStyles.colors.PRIMARY_RED
  },
  viewList:{
      flex:1
  },
    container: {
      flex: 1,
    },
    item: {
      flexDirection:'row',
      padding: '5%',
      justifyContent:'space-between',
      alignItems:'center',
      marginVertical: 2
    },
    title: {
      fontSize: 15,
      fontFamily: customStyles.fonts.FONT_BOLD
    },
    textView:{
        marginTop:10, 
        fontSize:25, 
        fontFamily: 
        customStyles.fonts.FONT_REGULAR, 
        color: 'white'
      },
    textEmail:{
      fontFamily: customStyles.fonts.FONT_REGULAR,
      fontSize:15,
      color: '#D97D7D'
    }

  });

export default VoucherConsultOptions
