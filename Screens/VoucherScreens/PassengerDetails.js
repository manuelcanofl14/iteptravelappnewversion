import React, {useContext} from 'react'
import { View, StyleSheet, Text, ScrollView } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import customStyles from '../../styles/styles'
import ajustarIdioma from '../../Types/settingsLanguage'
import ToursContext from '../../ToursAPI/toursContext'

const PassengerDetails = ({route})=>{
    const {idioma} = useContext(ToursContext)
    const titles= [ ajustarIdioma(idioma,'apellidos'),
                    ajustarIdioma(idioma,'genero'),
                    ajustarIdioma(idioma,'nacimiento')
                    ,ajustarIdioma(idioma,'edad'),
                    ajustarIdioma(idioma,'pasaporte'),
                    ajustarIdioma(idioma,'pais'),
                    ajustarIdioma(idioma,'dieta'),
                    ajustarIdioma(idioma,'medical')]

    const {passengerParams} = route.params

    const details = (({surname,gender,birth,age,passport,country,dietary,medical})=>({surname,gender,birth,age,passport,country,dietary,medical}))(passengerParams)  
    const listDetails = Object.values(details) 

    return(
        <>
            <View style={styles.viewHeader}>
                <Ionicons name={'md-person-circle'} color={'#A3503F'} size={120}/>
                <Text style={styles.textHeader}>{passengerParams.name}</Text>     
            </View>
            <View style={{flex:1}}>
                <ScrollView>
                    {
                        listDetails.map((detail, index)=>{
                                return(
                                    <View style={styles.viewCard}>
                                        <Text style={styles.textTitle}>{titles[index]}</Text> 
                                        <Text style={styles.textDetail}>{detail}</Text>    
                                    </View>    
                                )
                        })
                    }
                </ScrollView>
            </View>
        </>
        
    )
}

const styles = StyleSheet.create({
    viewHeader:{
        height: 200,
        backgroundColor:'#ECECEC',
        justifyContent:'center',
        alignItems:'center'
    },
    textHeader:{
        fontFamily: customStyles.fonts.FONT_BOLD,
        color:'#A3503F',
        fontSize:14
    },
    viewCard:{
      backgroundColor:'#FFFFFF',
      borderColor: '#95989A',
      justifyContent:'space-between',
      flexDirection:'row',
      alignItems:'center',
      borderWidth:1,
      paddingHorizontal:'5%',
      height: 80  
    },
    textTitle:{
      fontFamily: customStyles.fonts.FONT_BOLD,
      fontSize:15,
      color:customStyles.colors.SECONDARY_RED  
    },
    textDetail:{
        color:'#707070',
        fontFamily:customStyles.fonts.FONT_BOLD,
        fontSize:15
    }
})

export default PassengerDetails