import React, {useContext} from 'react'
import { View, StyleSheet, Text, SafeAreaView, ScrollView, Image } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import customStyles from '../../styles/styles'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { formatText } from '../../helpers/formatText'
import ajustarIdioma from '../../Types/settingsLanguage'
import ToursContext from '../../ToursAPI/toursContext'

const VoucherDetails = ({route})=>{
    const {idioma} = useContext(ToursContext)
    const {voucherParams} = route.params
    const {titulo, tourDetalles } = voucherParams
    return(
        <>
            <SafeAreaView >
                <LinearGradient
                    colors={['#F52F57', '#FC5F5F', '#C90000' ]}
                    style={styles.viewBackRed}
                    start={{ x: 0, y: 0.5 }}
                    end={{ x: 1, y: 0.5 }}
                >
                    <View style={styles.viewTitle}>
                        <Text style={styles.textTitle}>{titulo}</Text>
                    </View>
                </LinearGradient>
                
                <View style={styles.viewContent}>
                    <View style={styles.viewLogo}>
                            <Image source={require('../../assets/ITEP2.png')} style={styles.viewLogo} />
                    </View>
                    <View style={styles.viewIconText}>
                        <MaterialIcons name={'place'} color={customStyles.colors.PRIMARY_RED} size={20}/>
                        <Text style={styles.textDetails}>{ajustarIdioma(idioma,'detallesTour')}</Text>
                    </View>
                    <ScrollView 
                        showsVerticalScrollIndicator={false}
                    >      
                        <Text style={{ fontFamily: customStyles.fonts.FONT_BOLD}}>{formatText(tourDetalles)}</Text>
                    </ScrollView>
                </View>

            </SafeAreaView>
        </>
    )
}

const styles = StyleSheet.create({
    viewBackRed:{
        width: '100%',
        height: '70%',
        backgroundColor: '#C90000',
        borderBottomLeftRadius:100,
        borderBottomRightRadius:100,
        justifyContent:'center'
    },
    viewLogo:{
        position:'absolute',
        top:'2%',
        right:'5%',
        width:50,
        height:50,

    },
    viewIconText:{
         flexDirection:'row' 
    },
    textTitle: {
       fontFamily: customStyles.fonts.FONT_REGULAR,
       fontSize:20,
       color: '#FFFFFF',
       textAlign:'center'
      },
      textDetails:{
        fontSize:20,
        color: customStyles.colors.PRIMARY_RED,
        fontFamily: customStyles.fonts.FONT_BOLD
      },
    viewTitle:{
        justifyContent:'center',
        alignItems:'center'
    },
    viewContent:{
        backgroundColor:'#FFFFFF',
        borderRadius:20,
        position:'absolute',
        width:'80%',
        height:'90%',
        top:'50%',
        left:'10%',
        alignItems:'center',
        padding:15
    }
})

export default VoucherDetails