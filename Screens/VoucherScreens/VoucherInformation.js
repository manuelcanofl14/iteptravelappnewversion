import React, {useContext} from 'react'
import { View, StyleSheet, Text, Image, ActivityIndicator } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { useFetch } from '../../helpers/hooks/useFetch'
import customStyles from '../../styles/styles'
import RowTextTitle from '../../UI/voucher/RowTextTitle'
import ajustarIdioma from '../../Types/settingsLanguage'
import ToursContext from '../../ToursAPI/toursContext'

const VoucherInformation = ({route})=>{
    const {idioma} = useContext(ToursContext)
    const {voucherParams} = route.params
    const {idVoucher, number, agencia, name, departure, hotel, emission} = voucherParams
    const {data} = useFetch('information','idVoucher', idVoucher)

    const information =  data? Object.values((({trip, tipotour, t_service, additional, language, observations})=>({trip, tipotour, t_service, additional, language, observations}))(data)) : []
    const schedule = data? Object.values((({d_in, d_out, t_in})=>({d_in, d_out, t_in}))(data)): []
    const accomodation = data? Object.values((({hotel, restaurant})=>({hotel, restaurant}))(data)): []
    
    const infoTitles = [
        ajustarIdioma(idioma,'trip'),
        ajustarIdioma(idioma,'tipotour'),
        ajustarIdioma(idioma,'tiposervi'),
        ajustarIdioma(idioma,'adicional'),
        ajustarIdioma(idioma,'language'),
        ajustarIdioma(idioma,'observacion')]

    const scheduleTitles = [
        ajustarIdioma(idioma,'fechain'),
        ajustarIdioma(idioma,'fechasal'),
        ajustarIdioma(idioma,'pickup')]

    const accomodationTitles = ['Hotel','Restaurant']

    return(
        <>
                <LinearGradient
                    colors={['#F52F57', '#FC5F5F', '#C90000' ]}
                    style={styles.viewBackRed}
                    start={{ x: 0, y: 0.5 }}
                    end={{ x: 1, y: 0.5 }}
                >
                    <View style={styles.viewImage}>
                        <View >
                            <Text style={styles.textTitleM}>{ajustarIdioma(idioma, 'codigo')}</Text>
                            <Text style={styles.textNumber}>{number}</Text>
                        </View>
                        <Image style={styles.image} source={require('../../assets/voucher.png')}/>
                    </View>
                </LinearGradient>
                <View style={styles.viewAgencyDetails}>
                    <View style={{justifyContent:'space-evenly'}}>
                        <View>
                            <Text style={styles.textTitle}>{ajustarIdioma(idioma,'agencia')}</Text>
                            <Text style={styles.textValue}>{agencia}</Text>
                        </View>
                        <View>
                            <Text style={styles.textTitle}>{ajustarIdioma(idioma,'lider')}</Text>
                            <Text style={styles.textValue}>{name}</Text>
                        </View>
                        <View>
                            <Text style={styles.textTitle}>{ajustarIdioma(idioma,'fecha')}</Text>
                            <Text style={styles.textValue}>{departure}</Text>
                        </View>
                    </View>
                    <View style={{marginLeft:20, justifyContent:'space-evenly'}}>
                        <View>
                            <Text style={styles.textTitle}>Hotel</Text>
                            <Text style={styles.textValue}>{hotel}</Text>
                        </View>
                        <View>
                            <Text style={styles.textTitle}>{ajustarIdioma(idioma,'emision')}</Text>
                            <Text style={styles.textValue}>{emission}</Text>
                        </View>
                    </View>
                </View>

               { data? <View style={styles.viewBottom}>
                            <View style={styles.viewInformation}>
                                <Text style={[styles.textTitle, {textAlign:'center'}]}>{ajustarIdioma(idioma,'Tinformacion')}</Text>
                               {
                                   information.map((info, index)=>{
                                        return(
                                            <RowTextTitle title={infoTitles[index]} value={info}/>
                                        )
                                   })
                               }
                            </View>
                            <View style={{flexDirection:'column'}}>
                                <View style={styles.viewSchedule}>
                                <Text style={[styles.textTitle, {textAlign:'center'}]}>{ajustarIdioma(idioma,'horario')}</Text>
                                    {
                                        schedule.map((schedule, index)=>{
                                            return(
                                                <RowTextTitle title={scheduleTitles[index]} value={schedule}/>  
                                            )
                                        })
                                    }
                                </View>
                                <View style={styles.viewSchedule}>
                                <Text style={[styles.textTitle, {textAlign:'center'}]}>{ajustarIdioma(idioma,'hospe')}</Text>
                                    {
                                        accomodation.map((accom, index)=>{
                                            return(
                                                <RowTextTitle title={accomodationTitles[index]} value={accom}/>  
                                            )
                                        })
                                    }
                                </View>
                            </View>
                        </View>
                :
                    <View style={styles.contain}>
                        <ActivityIndicator size="large" color={customStyles.colors.PRIMARY_RED} />
                    </View>
                }
        </>
    )
}

const styles = StyleSheet.create({
    contain: {
        flex: 1,
        justifyContent: "center",
        alignItems:'center'   
    },
    viewBackRed:{
            width: '100%',
            height: '50%',
            backgroundColor: '#C90000',
            borderBottomLeftRadius:100,
            borderBottomRightRadius:100,
            justifyContent:'center'
        },
    viewImage:{
            flexDirection:'row',
            alignItems:'center',
            justifyContent:'center',
            marginHorizontal:'5%',
            borderRadius:20,
            backgroundColor: customStyles.colors.PRIMARY_RED
        } ,   
    image:{
            width:100,
            height:100
        },
    textNumber:{
        color:'#FFFFFF',
        fontFamily: customStyles.fonts.FONT_BOLD,
        fontSize:15
    },
    textTitleM:{
        color:'#FFFFFF',
        fontFamily: customStyles.fonts.FONT_ITALIC,
        fontSize: 20,
        marginBottom:'5%'
    },
    viewAgencyDetails:{
        backgroundColor:'#FFFFFF',
        position:'absolute',
        width:'90%',
        height:'25%',
        top:'40%',
        left:'5%',
        borderRadius:20,
        flexDirection:'row',
        padding:'5%'
    },
    textTitle:{
        fontFamily: customStyles.fonts.FONT_BOLD,
        fontSize:15,
        color: customStyles.colors.PRIMARY_RED,
        marginBottom:2
    },
    textValue:{
        fontFamily: customStyles.fonts.FONT_BOLD,
        fontSize: 15,
        color: '#9A9A9A'
    },
    viewBottom:{
        marginTop:'33%',
        flexDirection:'row'
    },
    viewInformation:{
        backgroundColor:'#FFFFFF',
        marginLeft:'5%',
        justifyContent:'space-evenly',
        height:210,
        width:'50%',
        borderRadius:20
    },
    viewSchedule:{
        backgroundColor:'#FFFFFF',
        height: 100,
        width:  140,
        borderRadius:20,
        margin:'2%',
        justifyContent:'space-evenly',
    },
    viewInformationText:{
        flexDirection:'row',
         justifyContent:'space-between', 
         marginHorizontal:'10%'
        }
    })

export default VoucherInformation