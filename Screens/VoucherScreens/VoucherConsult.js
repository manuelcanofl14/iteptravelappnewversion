import React, {useState, useContext} from 'react'
import {Keyboard,View, Text, StyleSheet, Image, TextInput, Pressable} from 'react-native'
import customStyles from '../../styles/styles'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { useFetch } from '../../helpers/hooks/useFetch';
import ToursContext from '../../ToursAPI/toursContext'
import {showAlert} from '../../UI/voucher/AlertMessage'
import ajustarIdioma from '../../Types/settingsLanguage'

const VoucherConsult = ({navigation})=>{

    const [input, setInput] = useState('default')
    const {idioma} = useContext(ToursContext)

    const {data} = useFetch('voucher','email',input)

    const alert = ()=>{
        showAlert(ajustarIdioma(idioma,'errorVoucher'),ajustarIdioma(idioma,'entendido'))
    }
    const dataIsValid = (data)=>{
        const voucherData = data.filter((voucher)=>voucher.number != null)
        return voucherData
    }
    const consultVoucher = ()=>{
        if(input !=='default' && input!==''){
            if(data.length > 0){
                    const newVoucher = dataIsValid(data)[0]
                    navigation.navigate('voucherConsultOptions', {voucher: newVoucher})
                    Keyboard.dismiss() 
            }
            else{
                alert()
            }
        }
        else{
            alert()
            return;
        }   
    }
    
    return(
            <View style={styles.viewFlexLayout}>
              
              <View style={styles.viewRedHeader}>
                  <View style={styles.viewLogo}>
                      <Image style={styles.logo} source={require('../../assets/logoITEP.png')}  />
                  </View>
              </View>
              <View style={styles.viewContent}>
                  <View style={styles.ViewTextSearch}>
                      <Text style={styles.textSearch}>{ajustarIdioma(idioma,'buscar')}</Text>
                  </View>
                  <View style={styles.viewInput}>
                      <TextInput
                          style={styles.input}
                          placeholder={ ajustarIdioma(idioma, 'ingreseEmail')}
                          autoCapitalize={'none'}
                          keyboardType={'email-address'}
                          onChangeText={(e)=>setInput(e)}
                      />
                  </View>
                  <View style={styles.viewButton}>
                  <Pressable 
                      onPress={()=>{
                          consultVoucher()
                      }}
                      style={({ pressed }) => [
                          {
                            backgroundColor: pressed? customStyles.colors.OVERLAY_GRAY :customStyles.colors.PRIMARY_RED 
                          },
                          styles.wrapperCustom
                        ]}
                  >
                  
                      <Text style={styles.textButtonSearch}> {ajustarIdioma(idioma,'buscarBoton')}</Text>
                      <Text style={styles.searchIcon}>
                                  <FontAwesome5   name='search' size={10} />
                      </Text> 
                          
                  </Pressable >
                  </View>
              </View> 
                
             </View> 
       
    )
}


const styles = StyleSheet.create({
    viewFlexLayout:{
       flex:1,
       backgroundColor: customStyles.colors.OVERLAY_GRAY
    },
    viewRedHeader: {
        flex:2,
        backgroundColor: customStyles.colors.PRIMARY_RED,
        width:'100%',
        borderBottomLeftRadius: 60,
        borderBottomRightRadius:60
    },
    viewContent:{
        flex:3
    },
    viewLogo:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    logo:{
        height:'80%',
        width:'80%'
    },
    ViewTextSearch:{
        flex:2,
        paddingLeft:'10%',
        justifyContent:'flex-end',
        paddingBottom:'5%'
    },
    viewInput:{
        flex:1,
        paddingHorizontal:'10%',
        marginBottom:'5%'
    },
    viewButton:{
        flex:3,
        justifyContent:'flex-start',
        alignItems:'center',
        paddingTop:'5%'
    },
    input:{
        backgroundColor:'white',
        height:50,
        fontFamily: customStyles.fonts.FONT_ITALIC
    },
    textSearch:{
        color: customStyles.colors.PRIMARY_RED,
        fontFamily: customStyles.fonts.FONT_BOLD,
        fontSize: 20
    },
    searchIcon:{
        color: 'white'
    },

    textButtonSearch:{
        fontSize:12,
        fontFamily: customStyles.fonts.FONT_BOLD,
        color: '#FFFFFF'
    },
    wrapperCustom: {
        borderRadius: 8,
        padding: 6,
        width:'50%',
        height:'25%',
        borderRadius:10,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-evenly'
      }
})


export default VoucherConsult

