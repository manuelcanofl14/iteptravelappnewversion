import React from 'react'
import { View, StyleSheet, Text, SafeAreaView, ScrollView, ActivityIndicator, TouchableOpacity } from 'react-native'
import {useFetch} from '../../helpers/hooks/useFetch'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import customStyles from '../../styles/styles'

const PassengerList = ({route})=>{

    const {voucherParams} = route.params
    const {idVoucher, navigation} = voucherParams
    const {data} = useFetch('passenger','idVoucher', idVoucher)


    return(
        <SafeAreaView style={{marginTop:'10%'}}>
            <ScrollView>
                {
                    data?
                    data.map((passenger)=>{
                        return(
                            <TouchableOpacity 
                                style={styles.container}
                                onPress={()=>{navigation.navigate('passengerDetails', {passengerParams: passenger} )}}
                                >
                                <View style={styles.viewPassenger}>
                                    <Ionicons name={'md-person-circle'} color={'#A3503F'} size={40}/>
                                    <View >
                                        <Text style={styles.textName}>{passenger.name +' ' + passenger.surname}</Text>
                                        <Text style={styles.textCountry}>{passenger.country}</Text>
                                    </View>
                                </View>
                                <MaterialIcons name={'chevron-right'} color={'#A3503F'} size={40}/>
                            </TouchableOpacity> 
                        )    
                    })
                    : <View style={styles.contain}>
                        <ActivityIndicator size="large" color={customStyles.colors.PRIMARY_RED} />
                    </View>
                }
            </ScrollView>
           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    contain: {
            flex: 1,
            justifyContent: "center",
            alignItems:'center'   
    },
    container:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginVertical:'5%',
        paddingHorizontal:'5%',
        backgroundColor:'#FFFFFF',
        alignItems:'center',
        height: 100
    },
    viewPassenger:{
        flexDirection:'row',
        marginHorizontal:'5%',
        alignItems:'center'
    },
    textName:{
        fontFamily: customStyles.fonts.FONT_BOLD,
        fontSize:14,
        color: customStyles.colors.SECONDARY_RED
    },
    textCountry:{
        fontFamily: customStyles.fonts.FONT_REGULAR,
        fontSize:12,
        color: customStyles.colors.MEDIUM_GRAY
    }
})


export default PassengerList