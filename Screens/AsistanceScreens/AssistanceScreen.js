import React, {useContext} from 'react'
import {Text, StyleSheet, View, Image, ScrollView} from 'react-native'
import customStyles from '../../styles/styles'
import CardServiceIcon from '../../UI/service/CardServiceIcon'
import openWhatsApp from '../../helpers/openWhatsapp'
import toursContext from '../../ToursAPI/toursContext'
import ajustarIdioma from '../../Types/settingsLanguage'


const AssistanceScreen = ({navigation})=>{

    const {idioma} = useContext(toursContext)

    const items = [{icon:'tripadvisor', color:'#000000'},{icon:'location', color:'red'},{icon:'envelope', color:'yellow'},{icon:'whatsapp',color:'green'}]
    const titles = [ajustarIdioma(idioma,'resenas'),ajustarIdioma(idioma, 'ubicacion'),ajustarIdioma(idioma,'contacto'),ajustarIdioma(idioma,'whatsapp') ]

    const renderView = (index)=>{
        let url = ''
        if(index === 0){
            url = 'https://www.tripadvisor.com/UserReviewEdit-g294314-d1904909-ITEP_Eco_Travel-Cusco_Cusco_Region.html'
            navigation.navigate('webViewScreen', {webParams: {url}});
        }
        if(index === 1){
            navigation.navigate('webViewScreen');
        }
        if(index === 2){
            url = 'https://www.iteptravel.com'
            navigation.navigate('webViewScreen', {webParams: {url}});
        }
        if(index === 3){
            openWhatsApp(ajustarIdioma(idioma,'mensaje'),'51984947422', ajustarIdioma(idioma,'errorMsg'));
        }
    } 

    return(
        <>
        <View style={styles.viewContainer}>
            <View style={styles.viewHeader}>
                <Image  source={require('../../assets/ITEP2.png')}/>
                <Text style={styles.textTitle}>ITEP TRAVEL</Text>
                <Text style={styles.descriptionText}>
                    {
                        ajustarIdioma(idioma,'inicioAssist')
                    }
                </Text>
            </View>
            
            <View style={styles.viewButtons}>
            <Text style={styles.textTitleButton}>{ajustarIdioma(idioma,'assist')}</Text>

             <ScrollView  
                horizontal={true} 
                showsHorizontalScrollIndicator={false}
            >
                    {
                        items.map((item, index)=>{
                            return(
                                <CardServiceIcon 
                                    icon={item.icon} 
                                    text={titles[index]} 
                                    color={item.color} 
                                    renderView={()=>{renderView(index)}}
                                />
                            )
                        })
                    }
            </ScrollView>
                   
            </View>
        </View>
           
           
        </> 
    )
}

const styles = StyleSheet.create({
    viewContainer:{
        flex:1
    },
    viewHeader:{
        flex:2,
        alignItems:'center',
        justifyContent:'space-evenly'
    },
    viewButtons:{
        flex:1,
        backgroundColor: customStyles.colors.PRIMARY_RED
    },
    textTitle:{
        fontFamily: customStyles.fonts.FONT_BOLD,
        color: customStyles.colors.PRIMARY_RED,
        fontSize:20
    },
    descriptionText:{
        fontFamily: customStyles.fonts.FONT_REGULAR,
        color: '#8E8383',
        fontSize: 10
    },
    textTitleButton:{
        color:'#FFFFFF',
        fontSize:15,
        fontFamily: customStyles.fonts.FONT_BOLD,
        marginLeft:20,
        marginTop:20
    }
})

export default AssistanceScreen 