import React, {useState} from 'react'
import { Dimensions, View, StyleSheet, Text } from 'react-native'
import { WebView } from 'react-native-webview';
import MapView from 'react-native-maps'


const WebViewScreen = ({route})=>{

    const [location, setLocation] = useState({
        latitude: -13.5184,
        longitude: -71.9752,
        latitudeDelta: 0.0122,
        longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * 0.0122
})

    return(
        <>
            {
                route.params? <WebView
                source={{ uri: route.params.webParams.url }}
                style={{ marginTop: 20 }}
                />:
                <MapView 
                    initialRegion={location}
                    style={styles.map}
                >
                    <MapView.Marker coordinate={location} />    
                </MapView>
            }
        </>
       
    )
} 

const styles = StyleSheet.create({
    map:{
        width: '100%',
        height: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
})

export default WebViewScreen
