import React,{useContext} from 'react'
import { View, Text,Image, StyleSheet} from "react-native";
import customStyles from '../../styles/styles';
import { TabView, TabBar } from 'react-native-tab-view';
import TabViewScene from './TabViewScene';
import settingsLanguage from '../../Types/settingsLanguage';
import toursContext from '../../ToursAPI/toursContext'
import LikedHeart from '../../UI/tours/LikedHeart';

const TourDetails = ({route})=>{

    const {idioma, likedTours, addLikedTours} = useContext(toursContext)
    const {params} = route
    const tour = params.params
    const { img, title, duration, duration_n, price, type_of_tour, long_description, included, not_included, need} = tour


    const renderTabBar = props => (
        <TabBar
          {...props}
          bounces={true}
          activeColor={customStyles.colors.PRIMARY_RED}
          inactiveColor={customStyles.colors.PRIMARY_GRAY}
          labelStyle={{fontSize:10, fontFamily:customStyles.fonts.FONT_REGULAR}}
          pressColor={'#FFFFFF'}
          indicatorStyle={{ backgroundColor: customStyles.colors.PRIMARY_RED }}
          style={{height:'20%', backgroundColor: customStyles.colors.OVERLAY_GRAY }}
        />
      );

    const renderScene = ({ route }) => {
        switch (route.key) {
          case 'first':
            return <TabViewScene content={long_description} 
                                 icons={[{name:'sun',color:'#FCE300',text:duration},
                                         {name:'nightlight-round',color:'#B7DDFE', text:duration_n},
                                         {name:'attach-money',color:'#DC960A',text:price},
                                         {name:'hiking',color:'#BC26FB', text:type_of_tour}]}
                                 language={idioma}/>;
          case 'second':
            return <TabViewScene content={included} />;
          case 'third':
            return <TabViewScene content={not_included} />;
          case 'fourth':
            return <TabViewScene content={need} />;  
          default:
            return null;
        }
      };
      

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: settingsLanguage(idioma, 'overview'  ) },
        { key: 'second', title: settingsLanguage(idioma, 'incluye'  ) },
        { key: 'third', title: settingsLanguage(idioma, 'noIncluye'  ) },
        { key: 'fourth', title: settingsLanguage(idioma, 'necesita'  ) }
    ]);


    
    return(
        <>
            <Image source={{uri:`https://agency.iteptravel.com/saav/rec/img/${img}`}} style={styles.image} />
            <View style={styles.viewText}>
                <Text style={styles.textTitle}>{title}</Text>
            </View>
            
              <LikedHeart size={50} likedTours={likedTours} addLikedTours={addLikedTours} tour={tour} />
      
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                renderTabBar= {renderTabBar}
            />
        </>
    )
}


const styles = StyleSheet.create({
    image:{
        height: 350,
        width:'100%'
    },
    textTitle:{
        fontFamily:customStyles.fonts.FONT_BOLD,
        color: customStyles.colors.MEDIUM_GRAY,
        fontSize:20,
        fontWeight:'bold',
        textAlign:'center'
    },
    viewText:{
        width:250 ,
        margin:15,
        flexDirection:'row', 
        justifyContent:'center'
    }
})

export default TourDetails

