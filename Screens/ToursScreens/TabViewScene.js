import React from 'react'
import { View, Text, StyleSheet, ScrollView } from 'react-native'
import customStyles from '../../styles/styles'
import DetailsIcon from '../../UI/tours/DetailsIcon'
import {formatText} from '../../helpers/formatText'


const TabViewScene = ({content, icons, language })=>{
    return (
        <>
            <ScrollView>

               {icons && <DetailsIcon icons = {icons} language={language} />}
                <View style={styles.viewContent}>
                    <Text style={styles.text}>
                        {formatText(content)}
                    </Text>
                </View>
            
            </ScrollView>

        </>
        ) 

}

const styles = StyleSheet.create({
    viewContent:{
        margin:20,
        backgroundColor: customStyles.colors.SECONDARY_GRAY,
        paddingHorizontal:20
    },
    text:{
        fontFamily: customStyles.fonts.FONT_REGULAR,
        fontSize: 15,
        color: customStyles.colors.MEDIUM_GRAY

    }
})


export default TabViewScene

