import  React  from 'react'
import {View, StyleSheet, Image, Text, SafeAreaView} from 'react-native'
import customStyles from '../../styles/styles';
import CustomButton from '../../UI/tours/CustomButton'
import LikedHeart from '../../UI/tours/LikedHeart';

const Tour = ({tour, navigation, liked, likedTours})=>{

    
    const {img,title, activity, short_description, languaje } = tour


    return(
        <SafeAreaView style={{backgroundColor: customStyles.colors.SECONDARY_GRAY}}>
            <View style={styles.viewTour}>
                <Image style={styles.image}  source={{uri:`https://agency.iteptravel.com/saav/rec/img/${img}`}} /> 
                <View style={styles.viewInfo}>
                    <Text style={styles.textTitle}>{title.toUpperCase()}</Text>
                    <Text style={styles.textHigh}>{activity}</Text>
                    <Text style={styles.textDescr}>{short_description}</Text>
                    <CustomButton nav={{navigation, params:tour, screen:'TourDetails'}} language={{idioma:languaje,title:'ver'}} size={{width:100, height:25}} />
                </View>
                <View style={styles.viewIcon}>
                    <LikedHeart size={20} likedTours={likedTours} addLikedTours={liked} tour={tour} />
                </View>
                   
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    viewTour:{
        height:140,
        width: '90%',
        flexDirection:'row',
        backgroundColor: 'white',
        borderColor: customStyles.colors.PRIMARY_RED,
        borderWidth:2,
        alignSelf:'center',
        margin:10,
        borderRadius:15
    },
    image:{
        borderRadius:10,
        flex:2
    },
    viewInfo:{
    flex:3,
    alignItems:'center',
    justifyContent:'space-evenly',

    },
    viewIcon:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    textTitle: {
        fontSize:10,
        fontFamily: customStyles.fonts.FONT_REGULAR,
        textAlign:'center',
        fontWeight:'bold'
    }
    ,
    textHigh:{
        fontSize:8, 
        fontFamily: customStyles.fonts.FONT_REGULAR,
        textAlign:'left',
        color: customStyles.colors.MEDIUM_GRAY
    },
    textDescr:{
        fontSize:7,
        fontFamily: customStyles.fonts.FONT_REGULAR,
        textAlign:'justify',
        marginHorizontal:10
    }
})

export default Tour