import React,{useContext, useEffect, useState} from 'react'
import { Text, StyleSheet , SafeAreaView, View, FlatList, TouchableHighlight} from 'react-native'
import ToursContext from '../../ToursAPI/toursContext'
import customStyles from '../../styles/styles'
import ajustarIdioma from '../../Types/settingsLanguage'
import CustomIcon from '../../UI/Icons/CustomIcon'
import CustomInput from '../../UI/tours/CustomInput'
import Tour from './Tour'

const SearchTours =({route, navigation})=>{

   const [showSearchBar, setShowSearchBar] = useState(false)
   const [listTours, setListTours] = useState([])

   const {input, isCategory} = route.params
   const {idioma, listaIdioma, addLikedTours, likedTours} = useContext(ToursContext)


   const searchTours = (input, isCategory)=>{
    const inputLower = input.toLowerCase()
    return  (isCategory)?listaIdioma.filter(({type_of_tour})=>type_of_tour.toLowerCase().includes(inputLower) ):
                         listaIdioma.filter(({title})=>title.toLowerCase().includes(inputLower) )
   }
   
   
   useEffect(()=>{
    setListTours(searchTours(input, isCategory))

   },[input])

   navigation.setOptions({
    headerRight: () => (
        <View style={showSearchBar && { width:'88%'}} >
            { showSearchBar? 
                <CustomInput props={{navigation,idioma}}/>:

                <TouchableHighlight
                    onPress={()=>setShowSearchBar(!showSearchBar)}
                    underlayColor={customStyles.colors.OVERLAY_GRAY}
                    activeOpacity={1}
                >
                <CustomIcon 
                    name={'search'} 
                    color={customStyles.colors.MEDIUM_GRAY} 
                    size={30}
                />
                </TouchableHighlight> 
            }
        </View>)
        });


    return (
        <>
                <SafeAreaView>
                        
                        <Text style={styles.textTitle}> {`${ajustarIdioma(idioma, 'resultadoBusq')}  ${listTours.length}  Tours` }</Text>
                        <View  style={styles.viewText} >
                            <Text style={styles.inputTitleText}>{input.toUpperCase()}</Text>
                            <View style={styles.viewIcon} >
                                <CustomIcon  name={'horizontal-rule'} color={customStyles.colors.PRIMARY_RED} size={100}/>
                            </View>
                        </View>
                    
                </SafeAreaView>

                <FlatList
                    data={listTours}
                    renderItem={
                            ({item})=> <Tour tour={item} navigation={navigation} liked={addLikedTours} likedTours={likedTours} />
                    }
                    keyFactor={
                            (item)=> item.id
                    }
                />
        </>
            
    )
}


const styles = StyleSheet.create({
    textTitle:{
        color: customStyles.colors.SECONDARY_RED,
        fontFamily: customStyles.fonts.FONT_BOLD,
        fontSize: 15,
        alignSelf:'center',
        marginVertical:20,
        fontWeight:'bold'
    },
    inputTitleText:{
        fontFamily:customStyles.fonts.FONT_REGULAR,
        fontSize: 20,
        marginLeft:20
    },
    viewIcon:{
        position:'absolute',
        top: -20,
        left:5
    },
    viewText:{
        marginVertical:10
    }
   
})

export default SearchTours