import React, {useContext, useState, useEffect} from 'react'
import {StyleSheet,View, FlatList, Text, TouchableHighlight, ScrollView} from 'react-native'
import CustomInput from '../../UI/tours/CustomInput';
import {categories} from '../../DataFlow/tours/categories'
import CategoryIcon from '../../UI/tours/CategoryIcon';
import toursContext from '../../ToursAPI/toursContext';
import customStyles from '../../styles/styles';
import ListCards from '../../UI/tours/ListCards';
import ajustarIdioma from '../../Types/settingsLanguage';


  
const MainTours = ({navigation})=>{

    const {idioma, listaIdioma, likedTours } = useContext(toursContext);

    const [popular, setPopular] = useState([])

    useEffect(() => {
        //searchTours(listaIdioma, 'machu picchu')
        setPopular ( listaIdioma.filter(({title})=>title.toLowerCase().includes('machu picchu') ) ) 
    }, [])


    return(
        <ScrollView>

            <CustomInput 
              props={{navigation,idioma}}  
            />
            
                <FlatList
                    data={categories}
                    horizontal={true}
                    style={styles.viewIconList}
                    showsHorizontalScrollIndicator={false}
                    renderItem={ ({item})=>{
                        return(
                            <CategoryIcon props={{...item, idioma, navigation}} />
                        )}}
                    keyExtractor={(category)=> category.icon}
                >
                </FlatList>

                <Text style={styles.viewText}>{ajustarIdioma(idioma,'selectCatg' )}</Text>
                        
           <View style={styles.viewTitles} >
                   <Text style={styles.textTitle}>
                   {ajustarIdioma(idioma,'popDest')}        
                   </Text> 
                   <TouchableHighlight style={{alignSelf:'center'}}>
                        <Text style={[styles.textTitle, ,{fontSize:10}]}>
                            {ajustarIdioma(idioma,'seeAll')}     
                        </Text>  
                    </TouchableHighlight>      
           </View>                    

           <ListCards collection={ popular } />

            { likedTours.length >0 &&
                <View>
                    <View style={styles.viewTitles} >
                        <Text style={styles.textTitle}>
                                {ajustarIdioma(idioma,'meinT')}        
                        </Text> 
                    </View>     
            
                    <ListCards collection={ likedTours } />
                </View>
            }

        
        </ScrollView>
    
    )
}


const styles = StyleSheet.create({
    viewIconList:{
        marginVertical: 20,
        marginLeft: 20
    },
    viewText:{
        color: customStyles.colors.PRIMARY_GRAY,
        alignSelf:'center', 
        fontSize:15 
    },
    textTitle:{
       color : customStyles.colors.PRIMARY_RED,
       fontSize: 25,
       fontFamily: customStyles.fonts.FONT_BOLD
    },
    viewTitles:{
        flexDirection: 'row', 
        justifyContent:'space-between', 
        margin:20
    }
})

export default MainTours
