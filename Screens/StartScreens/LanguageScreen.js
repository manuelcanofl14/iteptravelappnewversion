import { useNavigation } from '@react-navigation/native'
import React, { useContext, useEffect, useState } from 'react'
import {Text, View, StyleSheet, Image, TouchableHighlight, ActivityIndicator} from 'react-native'
import customStyles from '../../styles/styles'
import ToursContext from '../../ToursAPI/toursContext'


const LanguageScreen = ()=>{

    const navigation = useNavigation()
    
    const {lista, establecerIdioma, obtenerTours} = useContext(ToursContext)
    
    const [state, setState] = useState(false)


    useEffect(  ()=>{
            try {
                obtenerTours();
                setState(true)
            } catch (error) {
                console.log(error)
            }
        
        }
    
        ,[]
    )
    

    return(
        <>
         {lista.length>0?<View style={styles.background}>
                <View style={styles.viewTextLanguage}>
                    <Text style={styles.textSelect}>
                        SELECT LANGUAGE
                    </Text>
                </View>
                <View style={styles.imageView}>
                <View style={styles.viewFlags}>
                    <TouchableHighlight 
                        onPress={()=>{
                            establecerIdioma('ENGLISH');
                            navigation.navigate('start')
                        }}
                        underlayColor={customStyles.colors.OVERLAY_GRAY}
                        activeOpacity={1}
                        >
                        <View
                            style={styles.viewFlag}
                        >
                            <Image source={require('../../assets/reino-unido.png')} style={styles.image} />
                            <Text style={styles.textLanguage}>English</Text>
                        </View>
                        
                    </TouchableHighlight>

                    <TouchableHighlight 
                        onPress={()=>{
                            establecerIdioma('ESPAÑOL'); 
                            navigation.navigate('start')
                        }}
                        underlayColor={customStyles.colors.OVERLAY_GRAY}
                        activeOpacity={1}
                        >
                        <View 
                            style={styles.viewFlag}
                        >
                            <Image source={require('../../assets/espana.png')} style={styles.image} />
                            <Text style={styles.textLanguage}>Español</Text>
                        </View>
                    </TouchableHighlight>
                </View>
                </View>
            </View>: <ActivityIndicator size="large" color="#640000" />}
        
        </>
    )
}


const styles = StyleSheet.create(
    {
        background:{
            flex:1,
            backgroundColor: customStyles.colors.SECONDARY_GRAY
        },
        viewTextLanguage:{
            alignItems: 'center',
            justifyContent: 'center',
            flex:2
        },
        viewFlags:{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center'
        },
        viewFlag:{
            flexDirection: 'column',
            alignItems: 'center'
        },
        textLanguage:{
            fontSize: 30,
            color: customStyles.colors.MEDIUM_GRAY,
            fontFamily: customStyles.fonts.FONT_REGULAR
        },
        textSelect:{
            fontSize: 40,
            color: customStyles.colors.PRIMARY_GRAY,
            textAlign: 'center',
            fontFamily: customStyles.fonts.FONT_REGULAR
        },
        imageView:{
            flex:2,
            alignItems: 'center',
            justifyContent: 'flex-start'
        },
        image:{
            height:100,
            width:100,
            marginHorizontal:15
        }
    }
)

export default LanguageScreen