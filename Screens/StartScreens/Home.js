import React,{useContext,useEffect} from 'react';
import {StyleSheet, View, Image, ImageBackground} from 'react-native'
import imageBackground from '../../assets/home-background.png'
import ToursContext from '../../ToursAPI/toursContext';
import CustomButton from '../../UI/tours/CustomButton';



const Home =({navigation})=>{

    const {idioma, cambiarIdioma} = useContext(ToursContext)

    useEffect(
        ()=>{
            try {
                cambiarIdioma();
            } catch (error) {
                console.log(error)
            }
        
        },[]
    )
    

    return (
        <ImageBackground
            
                source={imageBackground}
                style={styles.viewBackground}
        >
        <View
            style={styles.viewMainContent}
        >
            <Image
                source={require('../../assets/logoITEP.png')}
                style={styles.viewLogo}
            />

        <CustomButton nav={{navigation, screen:'mainTours'}} language={{idioma,title:'empezar'}} size={{width:150, height:75}} />
    
        </View>

           
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    viewBackground: {
        height: '100%',
        width: '100%'
        
    },
    viewMainContent:{
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'flex-start',
        left: 0,
        right: 0,
        top: 10,
        bottom: 0
    },
    viewLogo:{
        height: '40%',
        width: '70%'
    }
})


export default Home