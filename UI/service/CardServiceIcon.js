import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import customStyles from '../../styles/styles';
import CustomIcon from '../Icons/CustomIcon';

const CardServiceIcon = ({icon, text, color, renderView})=>{
    return(
        <TouchableOpacity 
            style={styles.viewIcon}
            onPress={()=>renderView()}
            >
            <CustomIcon name={icon} color={color} size={50} />
            <Text style={styles.textIcon} >{text}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    viewIcon:{
        backgroundColor:'#FFFFFF',
        borderRadius:18,
        height:'80%',
        width:100,
        justifyContent:'center',
        alignItems:'center',
        margin:10
    },
    textIcon:{
        textAlign:'center',
        color: customStyles.colors.PRIMARY_RED,
        fontFamily: customStyles.fonts.FONT_BOLD,
        fontSize:8
    }
})

export default CardServiceIcon