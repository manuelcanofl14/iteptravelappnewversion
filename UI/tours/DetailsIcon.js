import {View, Text, StyleSheet } from 'react-native'
import React from 'react'
import CustomIcon from '../Icons/CustomIcon'
import customStyles from '../../styles/styles'
import ajustarIdioma from '../../Types/settingsLanguage'


const DetailsIcon = ({icons, language})=>{

    return(
        <View style = {styles.iconsView}>
            {
                icons.map(
                    ({name,color,text})=>{
                        return(
                            <View style={styles.innerView}>
                                 <CustomIcon name={name} color={color} size={20}/> 
                                 <Text style={styles.textIcon}>{text}</Text>
                                 {name==="sun" && <Text style={styles.textIcon}>{ajustarIdioma(language, 'dia')}</Text>}
                                 {name==="nightlight-round" && <Text style={styles.textIcon}>{ajustarIdioma(language, 'noche')}</Text>}
                            </View>
                            
                        )
                    }
                )
            }
        </View>
        
    )
}


const styles = StyleSheet.create({
    iconsView:{
        flexDirection:'row',
        marginTop:30,
        paddingHorizontal:20
    },
    innerView:{
        flexDirection:'row',
         flex:1, 
         justifyContent:'space-evenly'
    },
    textIcon:{
        fontSize:10, 
        marginHorizontal:2,
        fontFamily: customStyles.fonts.FONT_REGULAR,
        alignSelf:'center'
    }
})

 export default DetailsIcon