import React, {useState,  useLayoutEffect} from 'react'
import { StyleSheet, TouchableHighlight } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import customStyles from '../../styles/styles';


const LikedHeart = ({size, likedTours, addLikedTours, tour })=>{

    const [isLikedNow, setLikedNow] = useState(false)
    const {id} = tour

    useLayoutEffect(()=>{
        likedTours.find((tour)=>tour.id===id) && setLikedNow(true)
    },[])

    useLayoutEffect(()=>{
        isLikedNow? addLikedTours(tour,true):addLikedTours(tour.id,false)
    },[isLikedNow])

    return(
        <>
            <TouchableHighlight
                style = { size>20 && styles.viewHeart }
                underlayColor={'white'}
                onPress = {()=>{ setLikedNow(!isLikedNow)  }}
            >
               
                <Ionicons  name={isLikedNow?'heart':'heart-outline'} color={customStyles.colors.PRIMARY_RED}  size={size}/>
            </TouchableHighlight>
        </>
    )
}


const styles = StyleSheet.create({
    viewHeart:{
        width:80, 
        height: 80,
        position:'absolute',
        top:300, 
        right:30,
        backgroundColor:'white', 
        alignItems:'center', 
        justifyContent:'center', 
        borderRadius:100,
        borderWidth:1.8,
        borderColor:customStyles.colors.PRIMARY_GRAY
    }
})

export default LikedHeart