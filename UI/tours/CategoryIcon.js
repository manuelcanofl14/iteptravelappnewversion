import React from 'react'
import {View, StyleSheet, Text, TouchableHighlight, Image} from 'react-native'

import CustomIcon from '../../UI/Icons/CustomIcon'
import ajustarIdioma from '../../Types/settingsLanguage'
import customStyles from '../../styles/styles'


const CategoryIcon = ( {props})=>{
    const {icon, color, size, title, idioma, navigation, picture} = props
    
    return(
        <View  style={styles.viewIcon}>
            <TouchableHighlight
                onPress={()=>navigation.navigate('searchTours', {isCategory:true, input: title })}
                underlayColor={customStyles.colors.OVERLAY_GRAY}
                activeOpacity={1}
            >
                <View style={styles.viewInnerIcon}>
                    {
                        icon?
                        <CustomIcon 
                        name={icon} 
                        color={color} 
                        size={size} />
                         :
                        <Image source={picture} style={styles.image} />
                    }
                    
                    <Text style={styles.viewText}>{ajustarIdioma(idioma,title )}</Text>
                </View>
            </TouchableHighlight>
        </View>

    )
}
const styles = StyleSheet.create({
    viewIcon:{
        backgroundColor: '#ffffff',
        borderRadius: 20,
        marginHorizontal:3,
        height: '100%'
    },
    viewInnerIcon:{
        alignItems: 'center'
    },
    viewText:{
        paddingHorizontal:10,
        fontFamily: customStyles.fonts.FONT_REGULAR,
        color: customStyles.colors.MEDIUM_GRAY
    },
    image:{
        width:100,
        height:100
    }

})

export default CategoryIcon