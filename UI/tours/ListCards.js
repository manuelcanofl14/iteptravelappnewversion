import React from 'react'
import { StyleSheet, View, ScrollView, Text } from 'react-native'
import CardTour from './CardTour'


const ListCards = ({collection})=>{
    return(
        collection.length>0?
        <View style={styles.viewMain}>
                <ScrollView horizontal={true}>
                    {
                        collection.map(
                            (item)=><CardTour tour={item} />
                        )
                    }
                </ScrollView>    
        </View>: <Text>loading</Text>
        
    )
}

const styles = StyleSheet.create({
   viewMain:{ marginLeft:20}
})

export default ListCards