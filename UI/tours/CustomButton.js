import React from 'react'
import {Text,StyleSheet, View,  TouchableHighlight} from 'react-native'
import customStyles from '../../styles/styles'
import ajustarIdioma from '../../Types/settingsLanguage'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native'

const CustomButton = ({nav,size, language})=>{

    const {navigation, params, screen} = nav
    const routes = useNavigation() 
    const {width, height} = size
    const {idioma, title} = language
   return(

        <TouchableHighlight
            onPress={()=>{routes.navigate(screen, {params:params})}}
            underlayColor = {customStyles.colors.SECONDARY_RED}
            style= {styles.button}
            enabled = {false}
            >
            <View style={[styles.viewButton,{ width: width, height:height, padding:  width/16 }]}>
                <Text style={[styles.textView,{fontSize: width/10} ]}>
                    {ajustarIdioma(idioma,title)}
                </Text>
                <FontAwesome5 name="chevron-right" size={width/10} color='white' />
            </View>

        </TouchableHighlight>
   ) 

}

const styles = StyleSheet.create({
    button:{
        borderRadius: 30
    },
    viewButton:{
        backgroundColor: customStyles.colors.PRIMARY_RED,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderRadius: 30
    },
    textView:{
        color: 'white',
        fontFamily: customStyles.fonts.FONT_BOLD,
        // marginRight:10,
        textAlign: 'center'
    },
})


export default CustomButton

