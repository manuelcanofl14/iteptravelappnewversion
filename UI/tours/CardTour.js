import React from 'react'
import { StyleSheet, View, Image, Text, Pressable } from 'react-native'
import customStyles from '../../styles/styles'
import { useNavigation } from '@react-navigation/native'


const CardTour = ({tour})=>{

  const routes = useNavigation()
    
  return (
        <Pressable 
            onPress={()=>routes.navigate('TourDetails', {params:tour})}
            android_ripple={{color:customStyles.colors.OVERLAY_GRAY, foreground:true, radius:170}}
        >
            <View >
                <Image style={styles.imageTour} source={{uri:`https://agency.iteptravel.com/saav/rec/img/${tour.img}` }} />
            </View>
            <Text style={styles.textStyle}> {tour.title} </Text>
        </Pressable> 
  )
}


const styles = StyleSheet.create({
    viewCard:{
        borderColor:'red',
        borderWidth:3,
        borderRadius:30,
        margin:5
    },
    imageTour:{
        width:250, 
        height:250, 
        borderRadius:30, 
        margin:5,
        opacity:0.7, backgroundColor: customStyles.colors.PRIMARY_RED 
    },
    textStyle:{
        position:'absolute',
        left:'10%',
        right:'10%',
        color:'white',
        bottom:'20%',
        fontSize:20,
        textAlign:'center',
        fontFamily: customStyles.fonts.FONT_BOLD
        // bottom:'50%'
    }
})


export default CardTour
