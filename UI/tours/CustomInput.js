import React, { useState } from 'react';
import {Keyboard, View, Text, StyleSheet, TextInput, TouchableWithoutFeedback} from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import customStyles from '../../styles/styles';
import ajustarIdioma from '../../Types/settingsLanguage';


const CustomInput = ({props})=>{

    const {navigation, idioma} = props

    const [input, setInput] = useState('')
    const [showEmpty, setShowEmpty] = useState(true)

    

    return(
        <View style={styles.viewInput}>
                <TextInput
                    style={styles.input}
                    onChangeText={
                        (e)=>setInput(e)}
                    value={input}
                    placeholder={ showEmpty? ajustarIdioma(idioma, 'buscar') : ajustarIdioma(idioma, 'errorInput')}
                    maxLength={20}
                />
              <TouchableWithoutFeedback
                onPress={()=>{
                    Keyboard.dismiss();
                    (input !=='')? ( navigation.navigate('searchTours', {input:input.trim()})  ): setShowEmpty(false)
                }}
              >
                    <Text style={styles.searchIcon}>
                                <FontAwesome5   name='search' size={20} />
                    </Text>      
              </TouchableWithoutFeedback>
            
        </View>     
    )
}

const styles = StyleSheet.create({
    input:{
        backgroundColor:'white',
        borderRadius:40,
        flex: 1,
        paddingHorizontal:40,
        fontFamily: customStyles.fonts.FONT_ITALIC
    },
    viewInput:{
        alignItems:'center',
        flexDirection:'row',
        marginVertical: 10,
        marginHorizontal:20
    },
    searchIcon:{
        color: customStyles.colors.PRIMARY_GRAY,
        position: 'absolute',
        right: '10%',
        top: '25%'
    }
})

export default CustomInput