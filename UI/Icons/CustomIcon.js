import React, {useState, useEffect} from 'react';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'


const iconSet = [{id:1,set: FontAwesome5} ,
                 {id:2, set: MaterialIcons}, 
                 {id:3, set: Entypo}, 
                 {id:4, set: MaterialCommunityIcons},
                 {id:5, set: Ionicons},{id:6, set: FontAwesome} ]


const getIconFontSet =  (name)=>{

                    const validName = iconSet.find((Icon)=>Icon.set.hasIcon(name)) 
                    if(validName){
                        return (validName.id)
                    }
                    else{
                        return 1
                    }
                   
     }



const CustomIcon = ({name,color, size})=>{

    const [icon, setIcon] = useState(1)

    useEffect( ()=>{
         setIcon(getIconFontSet(name)) 
    },[]) 

            {

                switch (icon) {
                case 1:
                return <FontAwesome5 name={name} color={color} size={size} />
                case 2:
                return <MaterialIcons name={name} color={color} size={size}/>
                
                case 3:
                return <Entypo name={name} color={color} size={size}/>
                
                case 4:
                return <MaterialCommunityIcons name={name} color={color} size={size}/>
                
                case 5:
                return <Ionicons name={name} color={color} size={size}/>

                case 6:
                return <FontAwesome name={name} color={color} size={size}/>
               
                default:
                    return <FontAwesome5 name={'location-arrow'} color={color} size={size} />
            }
        }
        
  
}

export default CustomIcon