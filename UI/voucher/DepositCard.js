import React from 'react'
import { View, StyleSheet, Text} from 'react-native'
import customStyles from '../../styles/styles';
import CustomIcon from '../../UI/Icons/CustomIcon'

const DepositCard =({deposit, titles})=>{

   const subDeposit = (({cantidad,deposito,detalle, descripcion, precio, subtotal }) => ({cantidad,deposito,detalle, descripcion, precio, subtotal }))(deposit);
   const  values = Object.values(subDeposit)

  

    return(
        <View style={styles.viewCard} index>
            {
                titles.map(
                    (title,index)=> {
                        
                        return(
                            <>
                            {
                              index===titles.length-1 &&  <View style={styles.viewLine}/>      
                            }
                             
                             <View style={[styles.viewRow, {backgroundColor:index!==0?'#FFFFFF':'#E2E2E2'}]}>
                                 <Text style={styles.textTitle}>
                                    {title}
                                </Text>
                                <Text style={[styles.textValue, , {color: !Number.isNaN(Number.parseFloat(values[index])) && customStyles.colors.PRIMARY_RED}]}>
                                     {(index===titles.length-1 || index===titles.length-2 || index ===1) && 
                                      <CustomIcon name={'attach-money'} 
                                            color={customStyles.colors.PRIMARY_RED} 
                                            size={10}/>}                                   
                                     {values[index]}
                                </Text>
                             </View>   
                            </>
                        )
                    }  
                )
            }
        </View>
    )
}
const styles = StyleSheet.create({
    viewCard:{
        backgroundColor:'#FFFFFF',
        marginHorizontal:'5%',
        marginVertical:'5%'
    },
    viewRow:{
        flexDirection:'row',
        marginBottom:'3%', 
        padding:'3%',
        alignItems:'center',
        justifyContent:'space-between'
    },
    textTitle: {
            fontFamily: customStyles.fonts.FONT_BOLD,
            fontSize: 10,
            color: '#707070'
        },
    textValue:{
        textAlign:'justify',
        color:'#000000',
        fontFamily: customStyles.fonts.FONT_BOLD,
        fontSize: 10
    },
    viewLine:{ 
        height:1, 
        backgroundColor: '#707070',
        marginHorizontal:'3%'
    },
    

})

export default DepositCard