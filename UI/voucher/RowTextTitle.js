import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import customStyles from '../../styles/styles'

const RowTextTitle = ({title, value})=>{
    return(
        <View style={styles.viewInformationText}>
                <Text style={styles.textTitle}>{title}</Text>
                <Text style={styles.textValue}>{value}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    textTitle:{
        fontFamily: customStyles.fonts.FONT_BOLD,
        fontSize:10,
        color: customStyles.colors.PRIMARY_RED
    },
    textValue:{
        fontFamily: customStyles.fonts.FONT_BOLD,
        fontSize: 10,
        color: '#9A9A9A'
    },
    viewInformationText:{
        flexDirection:'row',
         justifyContent:'space-between', 
         marginHorizontal:'10%'
        }
})

export default RowTextTitle