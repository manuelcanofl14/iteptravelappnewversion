import {  Alert } from 'react-native';

export const showAlert =(textoError, textoConfirmacion, funcion)=>{
    Alert.alert(
        "Error",
        textoError,
         [{text:textoConfirmacion,
           onPress: funcion
        }]
    )
}