import  {createContext} from 'react'

const ToursContext = createContext()

export default ToursContext
