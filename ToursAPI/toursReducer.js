import {OBTENER_TOURS, CAMBIAR_IDIOMA,OBTENER_TOURS_IDIOMA, ADD_LIKED_TOURS, DELETE_LIKED_TOURS} from '../Types/index'

export default (state, action) =>{
    switch(action.type){
        case OBTENER_TOURS:
            return{
                ...state,
                lista: action.payload
            }
        case CAMBIAR_IDIOMA:
            return{
                ...state,
                idioma: action.payload
            }
        case OBTENER_TOURS_IDIOMA:
            return{
                ...state,
                listaIdioma: action.payload
            }
        case ADD_LIKED_TOURS:
            return{
                ...state,
                likedTours: [...state.likedTours,action.payload]
            }
        case DELETE_LIKED_TOURS:
                return{
                    ...state,
                    likedTours: state.likedTours.filter((tour)=>tour.id !== action.payload)
                }
        default:
            return state;
    }
}

 