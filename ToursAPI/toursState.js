import React, {useReducer} from 'react'
import ToursContext from './toursContext'
import ToursReducer from './toursReducer'
import {OBTENER_TOURS, CAMBIAR_IDIOMA,OBTENER_TOURS_IDIOMA, ADD_LIKED_TOURS, DELETE_LIKED_TOURS} from '../Types/index'
import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'

const ToursState = props =>{

    //crear state inicial
    const initialState = {
        lista:[],
        idioma:'ENGLISH',
        listaIdioma:[],
        likedTours: []
    }

    //use reducer con dispacth para ejecutar las funciones
    const [state, dispatch] = useReducer(ToursReducer,initialState )

    // funcion para obtener los tours de la base de datos
    const obtenerTours = async ()=>{
        try {
            const url = 'https://incatrail.org/rest/post.php'
            const {data} = await axios.get(url)    
            dispatch({
                type:OBTENER_TOURS,
                payload: data
            })
        } 
        catch (error) {
            console.log(error)
        }
    }

    const cambiarIdioma = ()=>{
            const data = state.lista.filter(({languaje})=>{
                                return(           
                                    languaje === state.idioma       
                                )})
            dispatch({
                type: OBTENER_TOURS_IDIOMA,
                payload: data
            })
    }

    const establecerIdioma=(idioma)=>{
        dispatch({
            type:CAMBIAR_IDIOMA,
            payload: idioma
        })
    }

    const addLikedTours = async (tourId, isLiked)=>{
        try {
            if(isLiked){
                dispatch({
                    type: ADD_LIKED_TOURS,
                    payload: tourId
                })  
            }
            else{
                dispatch({
                    type: DELETE_LIKED_TOURS,
                    payload: tourId
                }) 
            }
         
          await AsyncStorage.setItem('tours', JSON.stringify(state.likedTours))
        } catch(e) {
          // save error
        }  
    }


      
    return (
        <ToursContext.Provider
            value={
                {
                    lista: state.lista,
                    obtenerTours,
                    idioma: state.idioma,
                    establecerIdioma,
                    listaIdioma: state.listaIdioma,
                    cambiarIdioma,
                    addLikedTours,
                    likedTours: state.likedTours
                }
            }
        >
            {props.children}
        </ToursContext.Provider>
    )
}

export default ToursState