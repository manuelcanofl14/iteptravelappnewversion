
export const categories =[
    {
        icon:'hiking',
        color:'blue',
        size:80,
        title: 'Adventure'  
    },
    {
        picture: require('../../assets/machu.png'),
        color:'red',
        size:80,
        title: 'Cultural'   
    },
    {
        picture: require('../../assets/hut.png'),
        color:'purple',
        size:80,
        title: 'Vivential Tourism'   
    },
    {
        picture: require('../../assets/fire.png'),
        color:'orange',
        size:80,
        title: 'Mistic Experience'   
    },
    {
        picture:require('../../assets/montana-arco-iris.png'),
        color:'yellow',
        size:80,
        title: 'Traditional'   
    },
    {
        picture:require('../../assets/guide.png'),
        color:'green',
        size:80,
        title: 'Others'  
    }
]

